import initialize from "./express/initialize";

const app = initialize();

app.listen(3000, () => console.log("Server started on port 3000"));
