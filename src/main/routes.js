import controller from "./controller";

const routes = [
  {
    method: "get",
    path: "/",
    handler: controller.getMain
  }
];

export default routes;
